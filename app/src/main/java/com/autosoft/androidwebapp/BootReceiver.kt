package com.autosoft.androidwebapp

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class BootReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent?) {
        try {
            RSS_Feed.checkRSS(context)
        } catch (ignore: Exception) {
        }
    }

}