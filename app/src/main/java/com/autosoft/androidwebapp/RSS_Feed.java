/*
 * Copyright (c) 2020 Felix Hollederer
 *     This file is part of GymWenApp.
 *
 *     GymWenApp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     GymWenApp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with GymWenApp.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.autosoft.androidwebapp;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Build;
import android.preference.PreferenceManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;
import androidx.core.content.ContextCompat;

import com.prof.rssparser.Article;
import com.prof.rssparser.Channel;
import com.prof.rssparser.OnTaskCompleted;
import com.prof.rssparser.Parser;

import java.util.List;
import java.util.Objects;

public class RSS_Feed {
    @NonNull
    public static final String CHANNEL_ID = "RSS_notification";

    public static final String rss_feed_Link = "https://asdoi.gitlab.io/rss-feeds/asdoiNews.xml";

    public static void checkRSS(@NonNull final Context context) {
        Parser parser = new Parser();
        parser.onFinish(new OnTaskCompleted() {

            //what to do when the parsing is done
            @Override
            public void onTaskCompleted(@NonNull Channel channel) {
                // Use the channel info
                List<Article> articles = channel.getArticles();
                if (articles.size() > 0) {
                    if (!Objects.requireNonNull(articles.get(0).getTitle()).equalsIgnoreCase(getLastLoadedRSSTitle(context))) {
                        //Send Notification
                        Intent notificationIntent = new Intent(context, MainActivity.class);
                        sendRSSNotification(articles.get(0), context, R.drawable.ic_rss_feed_black_24dp, notificationIntent);
                        setLastLoadedRSSTitle(articles.get(0).getTitle(), context);
                    }
                }
            }

            //what to do in case of error
            @Override
            public void onError(@NonNull Exception e) {
                // Handle the exception
            }
        });
        parser.execute(rss_feed_Link);
    }

    private static void sendRSSNotification(@Nullable Article article, @NonNull Context context, int smallIcon, @NonNull Intent notificationIntent) {
        if (article == null)
            return;

        int id = Objects.requireNonNull(article.getTitle()).hashCode();

        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntentWithParentStack(notificationIntent);
        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        createNotificationChannel(context);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(smallIcon)
                .setContentTitle(article.getTitle())
                .setContentText(article.getDescription())
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setStyle(new NotificationCompat.BigTextStyle())
                .setAutoCancel(true)
                .setOnlyAlertOnce(true)
                .setWhen(when)
                .setContentIntent(pendingIntent);

        if (notificationManager != null) {
            notificationManager.notify(id, notificationBuilder.build());
        }
    }

    private static void createNotificationChannel(@NonNull Context context) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, context.getString(R.string.rss_notification_channel), NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(context.getString(R.string.rss_notification_channel_description));
            channel.enableLights(true);
            channel.setLightColor(ContextCompat.getColor(context, R.color.colorAccent));
            channel.enableVibration(true);
            channel.enableVibration(true);
            channel.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION), null);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);

            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            Objects.requireNonNull(notificationManager).createNotificationChannel(channel);
        }
    }

    @Nullable
    public static String getLastLoadedRSSTitle(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("lastRSSTitle", null);
    }

    public static void setLastLoadedRSSTitle(String value, Context context) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("lastRSSTitle", value).apply();
    }
}
